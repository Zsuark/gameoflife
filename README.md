# GameOfLife
My experiments with John Conway's Game of Life

*** Important: JavaScript/ECMAScript 6 used ***
This means that as of time of writing the only supported browsers are Chrome and Firefox.
Tested in Chrome and Firefox under Mac OS 10.11

If you use the model on the server side, ensure your server side environment is capable of running ECMAScript 6. (E.g. Node 6)

A simple JavaScript 6 implementation of John Conway's Game Of Life.
This work is original and belongs to / copyright Raphael Krausz.

The rules of the game are simple, with each iteration:
	- Live cells with less than two living neighbours die
	- Live cells with two or three living neighbours remain alive
	- Live cells with more than three living neighbours die
	- Dead cells with three living neighbours become alive

More information may be found on Wikipedia: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
 
For this implementation, there are no out of bounds areas, when a cell is calculated that is beyond the boundary, that cell wraps to the other side of the board. (A toroidal array.)

Note: We are only tracking living cells, we do not track every cell.
We do not use a large array to track the state of every cell. It is unecessary, especially when relatively only a few cells are active.

===

Notes: Trying to keep a separation of concerns

GameOfLifeView.js  - Web page (view) javascript functions
GameOfLifeModel.js - Model javascript object and methods
