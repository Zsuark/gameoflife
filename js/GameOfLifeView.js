/*
 * GameOf.js
 * 
 * Author: Raphael Krausz <krauszraphael@gmail.com>
 * Date:   2016-09-06
 * Description:
 *
 * 		NB: This is the front end JavaScript for display layer
 *
 *		The actual iteration calculations are performed in GameOfLifeModel.js
 *
 *		So  GameOfLifeModel.js -> model object and methods
 *      and GameOfLifeView.js  -> functions for the view     
 *
 *  	NOTE: GameOfLifeModel.js MUST be loaded /before/ this script can be run!
 */

// NOTE: This game board is setup for a 40x60 grid - if you change this, you may also need to change the CSS.

/**** Globals section ****/

const GRID_HEIGHT = 40;
const GRID_WIDTH = 60;

/* CSS Identifiers constants for convenience */

const BOARD_ID = "#board"; // ID of the view's representation of the game board

// ID for the checkbox which decides if patterns add or replace the current game board
const CLEAR_ID = "#clear-board-for-patterns";

const ALL_CELLS = "[class$='-cell']";
const LIVE_CELL_CLASS = "living-cell";
const DEAD_CELL_CLASS = "dead-cell";

const CELL_ID_PREFIX = "cell-";
const PATTERN_ID_POSTFIX = "-pattern-button";

const ITERATION_TIMING = 50; //  the callback speed iterations occur at (used with setTimeout)



// Some global boolean statuses
var isRunning = false; // determines if iterations continue on a timeout loop

// the initial value of this will determine if the corresponding tickbox is checked on the webpage or not
// linked to the checkbox with CLEAR_ID
var clearBoardBeforeDrawing = true; // true - clear the board before adding a predefined "shape" - otherwise just add it

var userHasModifiedCells = false; // indicates if the board is out of sync with the GameOfLifeModel object

var gameModel; // the current instance of the GameOfLifeModel


/****  Upon document load - Initiatialise view and bindings  ****/

$(document).ready(function() {
	populateBoard();
	$(ALL_CELLS).click(toggleCellEventHandler);
	$(CLEAR_ID).prop('checked', clearBoardBeforeDrawing);

	$(CLEAR_ID).change(function() {
		clearBoardBeforeDrawing = $(this).is(':checked');
	});


	$("#start-stop-button").click(startStop);
	$("#single-step-button").click(singleStep);
	$("#reset-button").click(resetBoardView);

	$("[id$='" + PATTERN_ID_POSTFIX + "']").click(processPatternButtonPress);

});


/**** Main Functions ****/


// populateBoard - Populates the board with (dead) cells
// Should only be called once.
function populateBoard() {

	// For safety - don't run more than once.
	if ($(BOARD_ID).is(':parent')) return;

	for (var row = 0; row < GRID_HEIGHT; row++) {
		for (var column = 0; column < GRID_WIDTH; column++) {
			var cellId = CELL_ID_PREFIX + row + "-" + column;
			var divString = '<div id="' + cellId + '" class="' + DEAD_CELL_CLASS + '" />';

			$(BOARD_ID).append(divString);
		}
	}

	resetGameModel(); // instantiate gameModel variable

}

// resetGameModel - resets the model with the given set of live cells (or empty if none specified)
function resetGameModel(livingCellSet = new Set()) {
	gameModel = new GameOfLifeModel(livingCellSet, GRID_HEIGHT, GRID_WIDTH);
	userHasModifiedCells = false;
}

// toggleCellEventHandler - wrapper for toggleCell method from an event handler
function toggleCellEventHandler(event) {

	// As the user has modified cells in the view
	userHasModifiedCells = true;

	var cellId = event.target.id;
	toggleCell(cellId);
}


// toggleCell - toggles a cell between live/dead on the VIEW
function toggleCell(cellId) {

	if (!cellId.startsWith("#")) cellId = "#" + cellId;

	var classToAdd;
	var classToRemove;

	if ($(cellId).hasClass(DEAD_CELL_CLASS)) {
		classToAdd = LIVE_CELL_CLASS;
		classToRemove = DEAD_CELL_CLASS;
	} else {
		classToAdd = DEAD_CELL_CLASS;
		classToRemove = LIVE_CELL_CLASS;
	}

	$(cellId).removeClass(classToRemove).addClass(classToAdd);
}


// getCellModelIdFromCellViewId - convert a View cell id to the equivalent Model cell id
// e.g. "cell-5-10" -> "5,10"
function getCellModelIdFromCellViewId(cellViewId) {

	var cellModelId = cellViewId.replace(CELL_ID_PREFIX, "").replace("-", ",");

	return cellModelId;
}


// getCellViewIdFromCellModelId - convert a Model cell id to the equivalent View cell id
// e.g. "5,10" -> "cell-5-10"
function getCellViewIdFromCellModelId(cellModelId) {
	var cellViewId = CELL_ID_PREFIX + cellModelId.replace(",", "-");
	return cellViewId;
}


// clearBoardView - resets all living cells to dead in the web page VIEW
function clearBoardView() {
	$("." + LIVE_CELL_CLASS).each(function() {
		toggleCell(this.id);
	});
}


// resetBoardView - stops the game if it is running and clears the VIEW of the board
// Intended to be linked to a reset button - so we mark that the user has modified the board on the VIEW
function resetBoardView() {
	userHasModifiedCells = true;

	if (isRunning) startStop();
	clearBoardView();
}


// getLiveCellSetFromView - returns a set of strings of living cell coordinates
// e.g. {"5,10", "0,1", "21,10"}
function getLiveCellSetFromView() {
	var liveCellSet = new Set();
	$("." + LIVE_CELL_CLASS).each(function() {
		var cellId = getCellModelIdFromCellViewId(this.id);
		liveCellSet.add(cellId);
	});

	return liveCellSet;
}

// writeBoardViewtoBoardModel - if the user has changed the view
// make sure we write those changes to the board object
function writeBoardViewtoBoardModel() {

	if (userHasModifiedCells) {
		var liveCellSet = getLiveCellSetFromView();
		resetGameModel(liveCellSet);
		userHasModifiedCells = false;
	}

}

// iterate - performs an iteraction and updates the web page
// if the game is set run automatically, the method calls itself again after a set timeout
// Note: iterations will only continue whilst there are live cells on the board.
function iterate() {

	// if the user has modified the game board on the web page
	// reset the game board with the changes
	writeBoardViewtoBoardModel();

	var newLivingCellSet = gameModel.iterate();

	// if the resultant living cell set is empty - reset the game, and exit
	if (newLivingCellSet.size == 0) {
		resetBoardView(); // clears board, resets buttons, sets isRunning to false
		return;
	}

	// Otherwise update the board with the results
	updateViewFromModel(newLivingCellSet);


	// If the game is set to automatically execute, do so.
	if (isRunning) {
		setTimeout(iterate, ITERATION_TIMING);
	}
}

/**** functions to draw interesting starter shapes ****/

// Helper function to clear the board if needed when adding a pattern
// due to a pattern button press
function clearBoardCheck() {


	// reset the game board view if we are to clear it
	if (clearBoardBeforeDrawing) {
		resetGameModel();
		return;
	}

	// otherwise ensure we have applied any changes to the view to the
	// underlying board object

	writeBoardViewtoBoardModel();


	// if there is no need to clear the board, exit
	if (!clearBoardBeforeDrawing) return;

	// To clear the board, we will just simply reset the
	// GameOfLifeModel instance we have
	resetGameModel();


}


// updateViewFromModel - Helper function to write the live cells from the Model to the View
function updateViewFromModel(liveCellSet) {
	clearBoardView();

	liveCellSet.forEach(function(cellReference) {
		var cellId = "#" + getCellViewIdFromCellModelId(cellReference);
		toggleCell(cellId);
	});
}

// processPatternButtonPress - examines the ID of the button being pressed and dynamically figures
// out which pattern to insert into the game
// Should /ONLY/ be called from a button press of a pattern button.
function processPatternButtonPress(event) {

	// Check if the board needs to be cleared, clear or update the underlying object
	// as necessary
	clearBoardCheck();

	// run the specific function associated with the button press
	var buttonId = event.target.id.replace(PATTERN_ID_POSTFIX, "");

	switch (buttonId) {
		case "center-line":
			gameModel.addCenterLine();
			break;
		case "gosper-gun":
			gameModel.addGosperGun();
			break;
		case "acorn":
			gameModel.addAcorn();
			break;
		case "diehard":
			gameModel.addDiehard();
			break;
		default:
			alert("That button is not yet implemented!");
	}

	// update the view
	var currentLiveCellSet = gameModel.getLiveCells();
	updateViewFromModel(currentLiveCellSet);
}


/**** functions to control the game iterations via buttons ****/

// startStop - starts or stops the game from automatically iterating, updating the button labels and availabilty
// Note: the game automatically stops if the game board no longer has any live cells
function startStop() {
	if (isRunning) {
		// game is already running - stop it
		isRunning = false; // Note this will stop any further iterations happening
		$("#single-step-button").prop('disabled', false).css('opacity', 1);
		$("#start-stop-button").html('Start');
	} else {
		// game is stopped - let's start it up
		$("#start-stop-button").html('Stop');
		$("#single-step-button").prop('disabled', true).css('opacity', 0.5);
		isRunning = true;
		iterate();
	}
}

// singleStep - performs a single iteration
function singleStep() {
	// Should NOT be called if the game is already automatically iterating
	if (isRunning) return;

	iterate();
}