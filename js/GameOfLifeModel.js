/*
 * GameOfLifeModel.js
 * 
 * Author: Raphael Krausz <krauszraphael@gmail.com>
 * Date:   2016-09-06
 * Description:
 *
 * 		A simple JavaScript implementation of John Conway's Game Of Life.
 *      
 * 		This class is an abstraction of the board and cells and performs iterations.
 *  	
 * 		The rules of the game are simple, with each iteration:
 * 
 *			- Live cells with less than two living neighbours die
 *			- Live cells with two or three living neighbours remain alive
 *			- Live cells with more than three living neighbours die
 *			- Dead cells with three living neighbours become alive
 *
 *		More information may be found on Wikipedia: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
 *
 * 		For this implementation, there are no out of boards areas, when a cell is calculated that is beyond
 *		the boundary, that cell wraps to the other side of the board. (A toroidal array.)
 *      
 *      I use (row, column) notation - so the top left cell is marked as (0,0)
 *      So, say we have a 25x25 cell board cells go from (0,0) to (24,24) - then the neighbours of the
 *      cell at (0,0) are:
 *			(24,24), (24,0), (24,1) - the row above
 *          (0,24), (0,1) - same row
 *          (1,24), (1,0), (1,1) - the row below
 *
 * 		Note: We are only tracking living cells, we do not track every cell.
 * 		We do not use a large array to track the state of every cell. It is unecessary, especially
 * 		when relatively only a few cells are active.
 *
 * 		The default board size is 100 x 100 - provided for safety, but it really should be specified.
 *
 * 		There is a very basic test demo at the end, uncomment it to run - it writes to the JavaScript console.
 *
 *      Public functions:
 *			GameOfLifeModel - object representing the board
 *			GameOfLifeModel.iterate - perform an iteration
 *			GameOfLifeModel.getLiveCells - returns a Set of strings representing current live cells
 */

function GameOfLifeModel(liveCellSet = new Set(), totalRows = 100, totalColumns = 100) {
	// liveCellSet - Set of string representing live cells
	// totalRows - total number of rows on the Game of Life board
	// totalColumns - total number of columns on the Game of Life board

	/**** "private" functions ****/

	// Cell - object/function represents a cell on the Game of Life board
	// has row, column and cellId
	// cellId is a string representation in the form "row,column"
	function Cell(theRow, theColumn) {
		this.row = theRow % totalRows;
		this.column = theColumn % totalColumns;

		if (this.row < 0) this.row += totalRows;

		if (this.column < 0) this.column += totalColumns;

		this.cellId = "" + this.row + "," + this.column;
	} // End of Cell


	// makeCellFromId - returns a cell given a string representation of "row,column" where row and column are integers
	function makeCellFromId(cellId) {
		var coordinates = cellId.split(',');
		var row = parseInt(coordinates[0]);
		var column = parseInt(coordinates[1]);

		return new Cell(row, column);
	}


	// getCellNeighbours - returns an array of neighbouring cells (Cell inner "class")
	// the neighbouring cells returned do /not/ indicate if the cells are living or not
	function getCellNeighbours(theCell) {

		var neighbours = new Array();

		var cellRow = theCell.row;
		var cellColumn = theCell.column;

		for (i = -1; i < 2; i++) {

			var neighbourRow = cellRow + i;

			for (j = -1; j < 2; j++) {
				// Skip the cell itself
				if (i == 0 && j == 0) continue;

				var neighbourColumn = cellColumn + j;

				var neighbourCell = new Cell(neighbourRow, neighbourColumn);
				neighbours.push(neighbourCell);
			}
		}

		return neighbours;

	} // End of getCellNeighbours



	/**** "public"/"protected" functions ****/


	// iterate - performs an iteration and update the living cells.
	// To iterate we a) first check if the existing live cells remain alive or not; and
	// b) check those neighbouring cells - which aren't alive - if they come alive or not
	this.iterate = function() {
			var newLivingCellSet = new Set();

			var neighbourCellSet = new Set();


			liveCellSet.forEach(function(cellId) {
				var theCell = makeCellFromId(cellId);
				var neighbourCellArray = getCellNeighbours(theCell);

				var liveNeighbours = 0;

				neighbourCellArray.forEach(function(neighbourCell) {
					var neighbourCellId = neighbourCell.cellId;

					if (liveCellSet.has(neighbourCellId)) {
						// neighbour cell is included in the current living cells
						liveNeighbours += 1;

					} else {
						// neighbour cell is /not/ included in the current living cells
						neighbourCellSet.add(neighbourCellId);
					}
				});

				// Only live cells with 2 or 3 live neighbours remain alive
				if (liveNeighbours > 1 && liveNeighbours < 4) {
					newLivingCellSet.add(cellId);
				}
			});

			neighbourCellSet.forEach(function(cellId) {
				// for each of the neighbour cells listed, we count their neighbours
				// if there are exactly three living neighbours of the examined cell,
				// then the cell becomes alive.
				var theCell = makeCellFromId(cellId);
				var neighbourCellArray = getCellNeighbours(theCell);

				var liveNeighbours = 0;

				neighbourCellArray.forEach(function(neighbourCell) {
					var neighbourCellId = neighbourCell.cellId;
					if (liveCellSet.has(neighbourCellId)) {
						// neighbour cell is included in the current living cells
						liveNeighbours += 1;
					}
				});

				if (liveNeighbours == 3) {
					newLivingCellSet.add(cellId);
				}
			});

			liveCellSet = newLivingCellSet;
			return liveCellSet;
		} // End of this.iterate = function()


	// returns a Set of strings representing the live cells
	this.getLiveCells = function() {
			return liveCellSet;
		} // End of this.getLiveCells = function()


	// returns true if there are no live cells.
	this.isEmpty = function() {
		return liveCellSet.size == 0;
	}


	/**** Patterns and helpers ****/

	// takes an array of cell ids and adds them to the live cells
	function addCellArrayToLivingCells(cellArray) {
		for (var i in cellArray) {
			var theCell = makeCellFromId(cellArray[i]);
			liveCellSet.add(theCell.cellId);
		}
	}


	// Draws a vertical line across the middle of the grid
	this.addCenterLine = function() {

		var verticalMiddleRow = Math.round(totalRows / 2);

		for (var column = 0; column < totalColumns; column++) {

			var theCell = new Cell(verticalMiddleRow, column);
			liveCellSet.add(theCell.cellId);
		}
	}


	// Draws a Gosper Glider Gun in the top left
	this.addGosperGun = function() {

		var gosperGunReferenceArray = [
			"1,25",
			"2,23", "2,25",
			"3,13", "3,14", "3,21", "3,22", "3,35", "3,36",
			"4,12", "4,16", "4,21", "4,22", "4,35", "4,36",
			"5,1", "5,2", "5,11", "5,17", "5,21", "5,22",
			"6,1", "6,2", "6,11", "6,15", "6,17", "6,18", "6,23", "6,25",
			"7,11", "7,17", "7,25",
			"8,12", "8,16",
			"9,13", "9,14"
		];

		addCellArrayToLivingCells(gosperGunReferenceArray);
	}

	// Places an "Acorn" on the board
	this.addAcorn = function() {

		var acornReferenceArray = [
			"21,22",
			"22,24",
			"23,21", "23,22", "23,25", "23,26", "23,27"
		];

		addCellArrayToLivingCells(acornReferenceArray);
	}

	// Draws a "Diehard" on the board
	this.addDiehard = function() {

		var dieHardReferenceArray = [
			"31,37",
			"32,31", "32,32",
			"33,32", "33,36", "33,37", "33,38"
		];

		addCellArrayToLivingCells(dieHardReferenceArray);
	}


} // End of function GameOfLifeModel(...)


/* Simple Test suite */

/* * / // Remove space between the second star and slash to enable the test demo

function testDemo() {

	var liveCellsTestSet = new Set(["24,0", "0,0", "1,0"]);

	var testBoard = new GameOfLifeModel(liveCellsTestSet);

	var livingCellSet = testBoard.getLiveCells();

	// display helper function - writes current living cells to the console
	function display() {
		livingCellSet.forEach(function(cellId) {
			console.log(cellId);
		});
	}


	console.log('*** No iterations yet ***');
	display();


	livingCellSet = testBoard.iterate();
	console.log('*** first Iteration complete ***');
	display();


	livingCellSet = testBoard.iterate();
	console.log('*** second Iteration complete ***');
	display();

	livingCellSet = testBoard.iterate();
	console.log('*** third Iteration complete ***');
	display();


} // End of testDemo()

testDemo();

/* leave this comment here to allow easy enabling/disabling of test suite */